//
//  DoubleDispatch.cpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#include "DoubleDispatch.hpp"

void ReactVisitor::Visit( Wall *w )
{
	person->Wall();
}

void ReactVisitor::Visit( Coin *c )
{
	person->Coin();
}

void ReactVisitor::Visit(Cherry* ch)
{
	person->Cherry();
}

void ReactVisitor::Visit(Obstacle* os)
{
	person->Obstacle();
}

void ReactVisitor::Visit(Gate* g)
{
	person->Gate();
}


