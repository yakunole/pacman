//
//  Game.cpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#include "Game.hpp"

Map* map;
Person* p;
AssetManager* assets;
KeyboardController* controller;
Pacman* player;
Purple* purple;
GhostHouse* ghosthouse;
Mix_Music *theme;
Yellow* yellow;
Blue* blue;
Red* red;
UILabel* Point;
int points;

Game::Game()
{
	
}

Game::~Game()
{

}

void Game::Init( const char *title, int x, int y, int width, int height )
{
	if ( SDL_Init( SDL_INIT_EVERYTHING  ) == 0)
	{
		window = SDL_CreateWindow(title, x, y, width, height, 0);
		renderer = SDL_CreateRenderer( window, -1, 0);
		if ( renderer )
		{
			SDL_SetRenderDrawColor( renderer ,  0, 0, 0, 0);
		}
		isRunning = true;
	}
	
	if(TTF_Init() == -1)
	{
		std::cout << "error";
	}
	
	assets = new AssetManager();
	assets->AddTexture("pacman", "src/Assets/pacman.png", renderer);
	assets->AddTexture("menu", "src/Assets/output-onlinepngtools.png", renderer);
	assets->AddTexture("purple", "src/Assets/ghostpurple.png", renderer);
	assets->AddTexture("red", "src/Assets/ghostred.png", renderer);
	assets->AddTexture("yellow", "src/Assets/ghostyellow.png", renderer);
	assets->AddTexture("blue", "src/Assets/ghost.png", renderer);
	assets->AddTexture("wall", "src/Assets/wall.png", renderer);
	assets->AddTexture("gate", "src/Assets/gate.png", renderer);
	assets->AddTexture("coin", "src/Assets/coin.png", renderer);
	assets->AddTexture("cherry", "src/Assets/cherry-1.png.png", renderer);
	assets->AddTexture("scared", "src/Assets/Vulnerable-ghost-1.png.png", renderer);
	assets->AddTexture("obstacle", "src/Assets/New Piskel-1.png-2.png", renderer);
	assets->AddFont("arial", "src/Assets/SFNSMonoItalic.ttf", 20);
	assets->AddFont("snt", "src/Assets/Georgia.ttf", 40);
	
	ConfigurGame("src/Assets/configurefile.txt");
	
	if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0 )
		std::cout << "Wrong!";
	
	theme = Mix_LoadMUS("src/Assets/Pac-Man-Theme-Song.mp3");
	Mix_PlayMusic(theme, -1);
	
	map = new Map("src/Assets/16x16.map.txt");
	ghosthouse = new GhostHouse();
	map->LoadMap(36, 25, player, purple, red, yellow, blue, ghosthouse);
	
	controller = new KeyboardController(player);
	p = new Person();
	
	points = 0;
	SDL_Color white = {255, 102, 255};
	Point = new UILabel(20, 20, "Score: 0", "arial", white, assets, renderer);
}

void Game::HandleEvents()
{
	SDL_PollEvent(&event);
	switch (event.type)
	{
		case SDL_QUIT:
			isRunning = false;
			break;
		case SDL_KEYDOWN:
			if(event.key.keysym.sym == SDLK_ESCAPE)
			{
				int result = mainMenu();
				if(result == 0 )
					return;
			}
			break;
		default:
			break;
	}
}

void Game::Update()
{
	
	std::stringstream ss;
	ss << "Score: " << std::to_string(points);
	Point->SetLabelText(ss.str(), "arial");
	
	controller->Update(&event);
	player->Update();
	
	if( player->GetDest().x > 1152 )
		player->GetCoordinates()->GetPos().x = 0;
	
	else if( player->GetDest().x < 0 )
		player->GetCoordinates()->GetPos().x = 1152;
		
	
	for( auto it : map->GetWalls() )
	{
		if( Collision::AABB(player->GetCollision(), it->GetCollision()))
		{
			player->Hit();
		}
	}
	
	if( map->GetColliders().size() == 0)
		mainMenu();
	
	for( size_t i = 0 ; i < map->GetColliders().size() ; i++ )
	{
		if( map->GetColliders()[i] != nullptr )
		{
			
			if( Collision::AABB(player->GetCollision(), map->GetColliders()[i]->GetCollision()))
			{
				
			p->ReactTo(map->GetColliders()[i]);
			
			if(p->Result() == "obstacle" )
			{
				purple->isFrightened() = 1;
				red->isFrightened() = 1;
				yellow->isFrightened() = 1;
				blue->isFrightened() = 1;
				purple->isScattered() = 1;
				red->isScattered() = 1;
				yellow->isScattered() = 1;
				blue->isScattered() = 1;
			}
			
			if(p->Result() == "coin")
				points += 10;
			
			if(p->Result() == "cherry")
				points += 50;
				
			map->GetColliders()[i] = nullptr;
				
			}
		}
	}
	
	int a,b;
	
	
	a = player->GetDest().y/32;
	b = player->GetDest().x/32;
	
	if(player->GetDest().y % 32 > 29 && player->GetDest().y % 32 != 0 )
	{
		a = player->GetDest().y/32 + 1;
	}
	
	if(player->GetDest().x % 32 > 29 && player->GetDest().x % 32 != 0 )
	{
		b = player->GetDest().x/32 + 1;
	}
	
	
	purple->IncCounter();
	red->IncCounter();
	yellow->IncCounter();
	blue->IncCounter();
	
	if( purple->Time() )
	{
		purple->Update( map->GetMap(), a, b, purple->GetDestRect().y/32, purple->GetDestRect().x/32, player->GetDirection());
	}
	
	if( red->Time() )
	{
		red->Update(map->GetMap(), a, b, red->GetDestRect().y/32, red->GetDestRect().x/32, player->GetDirection());
	}
	
	if( yellow->Time() )
	{
		yellow->Update(map->GetMap(), a, b, yellow->GetDestRect().y/32, yellow->GetDestRect().x/32, player->GetDirection());
	}
	
	if( blue->Time() )
	{
		blue->Update(map->GetMap(), a, b, blue->GetDestRect().y/32, blue->GetDestRect().x/32, player->GetDirection());
	}
	
	if( purple->isFrightened() != 1 )
	{
		if( purple->GetCounter() == 64 )
		{
			purple->AnnulCounter();
			red->AnnulCounter();
			yellow->AnnulCounter();
			blue->AnnulCounter();
		}
	}

	else if( purple->isFrightened() == 1 && purple->GetCounter() == 64 )
	{
		purple->ftightenCounter();
		red->ftightenCounter();
		yellow->ftightenCounter();
		blue->ftightenCounter();
	}
	
	if( Collision::AABB(player->GetCollision(), purple->GetDestRect()))
	{
		if( purple->isFrightened() == 0 )
			mainMenu();
		else
		{
			purple->Init(ghosthouse->GetDestRect().x, ghosthouse->GetDestRect().y);
			points += 200;
		}
	}
	
	if( Collision::AABB(player->GetCollision(), red->GetDestRect()))
	{
		if( red->isFrightened() == 0 )
			mainMenu();
		else
		{
			red->Init(ghosthouse->GetDestRect().x, ghosthouse->GetDestRect().y);
			points += 200;
		}
	}
	
	if( Collision::AABB(player->GetCollision(), blue->GetDestRect()))
	{
		if( blue->isFrightened() == 0 )
			mainMenu();
		else
		{
			blue->Init(ghosthouse->GetDestRect().x, ghosthouse->GetDestRect().y);
			points += 200;
		}
	}
	
	if( Collision::AABB(player->GetCollision(), yellow->GetDestRect()))
	{
		if( yellow->isFrightened() == 0 )
			mainMenu();
		else
		{
			yellow->Init(ghosthouse->GetDestRect().x, ghosthouse->GetDestRect().y);
			points += 200;
		}
	}
	
}

void Game::Render()
{
	SDL_RenderClear( renderer );
	
	for(auto it : map->GetWalls() )
	{
		p->ReactTo(it);
		TextureManager::Draw(assets->GetTexture(p->Result()), renderer, it->GetSrcRect(), it->GetDestRect());
	}
	
	for(auto j : map->GetColliders() )
	{
		if( j != nullptr )
		{
			p->ReactTo(j);
			if( p->Result() == "cherry" )
			{
				if(  points > 400 )
					TextureManager::Draw(assets->GetTexture(p->Result()), renderer, j->GetSrcRect(), j->GetDestRect());
			}
			else
				TextureManager::Draw(assets->GetTexture(p->Result()), renderer, j->GetSrcRect(), j->GetDestRect());
		}
	}
	
	TextureManager::Draw(assets->GetTexture("pacman"), renderer, player->GetSource(), player->GetDest() );
	
	if( purple->isFrightened() == 1 )
		TextureManager::Draw(assets->GetTexture("scared"), renderer, purple->GetSrcRect(), purple->GetDestRect());
	else
		TextureManager::Draw(assets->GetTexture("purple"), renderer, purple->GetSrcRect(), purple->GetDestRect());
		
	if( blue->isFrightened() == 1 )
		TextureManager::Draw(assets->GetTexture("scared"), renderer, blue->GetSrcRect(), blue->GetDestRect());
	else
		TextureManager::Draw(assets->GetTexture("blue"), renderer, blue->GetSrcRect(), blue->GetDestRect());
	
	if( red->isFrightened() == 1 )
		TextureManager::Draw(assets->GetTexture("scared"), renderer, red->GetSrcRect(), red->GetDestRect());
	else
		TextureManager::Draw(assets->GetTexture("red"), renderer, red->GetSrcRect(), red->GetDestRect());
	
	if( yellow->isFrightened() == 1 )
		TextureManager::Draw(assets->GetTexture("scared"), renderer, yellow->GetSrcRect(), yellow->GetDestRect());
	else
		TextureManager::Draw(assets->GetTexture("yellow"), renderer, yellow->GetSrcRect(), yellow->GetDestRect());
	
	
	Point->Draw();
	
	SDL_RenderPresent( renderer );
}


void Game::Clean()
{
	delete p;
	delete assets;
	delete controller;
	delete purple;
	delete yellow;
	delete blue;
	delete red;
	delete map;
	delete ghosthouse;
	delete Point;
	SDL_DestroyRenderer( renderer );
	SDL_DestroyWindow( window );
	Mix_FreeMusic(theme);
	TTF_Quit();
	Mix_Quit();
	SDL_Quit();
}

void Game::ConfigurGame( const char* path )
{
	std::fstream ConfigureFile;
	std::string result, x, y;
	char a;
	ConfigureFile.open(path);
	
	for( int i = 0 ; i < 5 ; i++ )
		ConfigureFile.ignore();
	
	
	while( 1 )
	{
		ConfigureFile.get(a);
		if( a == '\n' )
			break;
		
		result += a;
	}
	
	player = new Pacman(std::stoi( result ));
	
	result.clear();
	
	for( int i = 0 ; i < 5 ; i++ )
		ConfigureFile.ignore();
	
	while( 1 )
	{
		ConfigureFile.get(a);
		if( a == ' ' )
			break;
		
		x += a;
	}
	
	while( 1 )
	{
		ConfigureFile.get(a);
		if( a == ' ' )
			break;
		
		y += a;
	}
	
	while( 1 )
	{
		ConfigureFile.get(a);
		if( a == '\n' )
			break;
		
		result += a;
	}
	
	purple = new Purple(std::stoi( x ), std::stoi( y ), std::stoi( result ));
	
	result.clear();
	x.clear();
	y.clear();
	
	for( int i = 0 ; i < 5 ; i++ )
		ConfigureFile.ignore();
	
	while( 1 )
	{
		ConfigureFile.get(a);
		if( a == ' ' )
			break;
		
		x += a;
	}
	
	while( 1 )
	{
		ConfigureFile.get(a);
		if( a == ' ' )
			break;
		
		y += a;
	}
	
	while( 1 )
	{
		ConfigureFile.get(a);
		if( a == '\n' )
			break;
		
		result += a;
	}
	
	red = new Red(std::stoi( x ), std::stoi( y ), std::stoi( result ));
	
	result.clear();
	x.clear();
	y.clear();
	
	for( int i = 0 ; i < 5 ; i++ )
		ConfigureFile.ignore();
	
	while( 1 )
	{
		ConfigureFile.get(a);
		if( a == '\n' )
			break;
		
		x += a;
	}
	
	yellow = new Yellow(std::stoi( x ));
	
	result.clear();
	x.clear();
	y.clear();
	
	for( int i = 0 ; i < 5 ; i++ )
		ConfigureFile.ignore();
	
	while( 1 )
	{
		ConfigureFile.get(a);
		if( a == ' ' )
			break;
		
		x += a;
	}
	
	while( 1 )
	{
		ConfigureFile.get(a);
		if( a == ' ' )
			break;
		
		y += a;
	}
	
	while( 1 )
	{
		ConfigureFile.get(a);
		if( a == '\n' )
			break;
		
		result += a;
	}
	
	blue = new Blue(std::stoi( x ), std::stoi( y ), std::stoi( result ));
	
	ConfigureFile.close();
}

bool Game::Running()
{
	return isRunning;
}

int Game::mainMenu()
{
	std::vector<UILabel*> labels;
	SDL_Color white = {255, 102, 255};
	SDL_Color greeb = {255, 255, 255};
	labels.push_back(new UILabel(500, 600, "EXIT", "snt", greeb, assets, renderer));
	labels.push_back(new UILabel(500, 500, "PLAY", "snt", white, assets, renderer));
	int x,  y;
	SDL_Event check;
	bool flag = true;
	while(1)
	{
		labels[0]->textColour = white;
		labels[0]->SetLabelText(labels[0]->labelText, labels[0]->labelFont);
		labels[1]->textColour = white;
		labels[1]->SetLabelText(labels[1]->labelText, labels[1]->labelFont);
		SDL_PollEvent(&check);
		switch (check.type)
		{
			case SDL_QUIT:
				flag = false;
				break;
			case SDL_MOUSEMOTION:
				x = check.motion.x;
				y = check.motion.y;
				for( int i = 0 ; i < 2 ; i++ )
				{
					if( x >= labels[i]->position.x && x <= labels[i]->position.x + labels[i]->position.w && y >= labels[i]->position.y && y <= labels[i]->position.h + labels[i]->position.y)
					{
						labels[i]->textColour =greeb;
						SDL_DestroyTexture(labels[i]->labelTexture);
						labels[i]->SetLabelText(labels[i]->labelText, labels[i]->labelFont);
					}
				}
				break;
			case SDL_MOUSEBUTTONDOWN:
				x = check.button.x;
				y = check.button.y;
				for( int i = 0 ; i < 2 ; i++ )
				{
					if( x >= labels[i]->position.x && x <= labels[i]->position.x + labels[i]->position.w && y >= labels[i]->position.y && y <= labels[i]->position.h + labels[i]->position.y )
					{
						SDL_Delay(300);
						for( size_t i = 0 ; i < labels.size() ; i++ )
							delete labels[i];
						if( i == 0 )
							isRunning = false;
						if( i == 1 )
						{
							Clean();
							Init( "Pacman" , SDL_WINDOWPOS_CENTERED , SDL_WINDOWPOS_CENTERED , 1152 , 800 );
						}
						return i;
					}
				}
				break;
		}
		if( flag == false )
			break;
		SDL_RenderClear( renderer );
		SDL_Rect a, b;
		a.x =  0;
		a.y = 0;
		a.w = 600;
		a.h = 275;
		b.x = 250;
		b.y = 100;
		b.w = 600;
		b.h = 275;
		TextureManager::Draw(assets->GetTexture("menu"), renderer, a, b);
		labels[0]->Draw();
		labels[1]->Draw();
		SDL_RenderPresent( renderer );
	}
	return 1;
}


