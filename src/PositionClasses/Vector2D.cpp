//
//  Vector2D.cpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#include "Vector2D.hpp"

Vector2D::Vector2D()
{
	x = 0;
	y = 0;
}

Vector2D::Vector2D( float x , float y )
{
	this->x = x;
	this->y = y;
}

Vector2D& Vector2D::Add(const Vector2D &vec)
{
	this->x += vec.x;
	this->y += vec.y;
	
	return *this;
}

Vector2D& Vector2D::Substract(const Vector2D &vec)
{
	this->x -= vec.x;
	this->y -= vec.y;
	
	return *this;
}

Vector2D& Vector2D::Multiply(const Vector2D &vec)
{
	this->x *= vec.x;
	this->y *= vec.y;
	
	return *this;
}

Vector2D& Vector2D::Divide(const Vector2D &vec)
{
	this->x /= vec.x;
	this->y /= vec.y;
	
	return *this;
}

Vector2D& operator+ ( Vector2D& vec1, Vector2D& vec2 )
{
	return vec1.Add(vec2);
}

Vector2D& operator- ( Vector2D& vec1, Vector2D& vec2 )
{
	return vec1.Substract(vec2);
}

Vector2D& operator* ( Vector2D& vec1, Vector2D& vec2 )
{
	return vec1.Multiply(vec2);
}

Vector2D& operator/ ( Vector2D& vec1, Vector2D& vec2 )
{
	return vec1.Divide(vec2);
}

Vector2D& Vector2D::operator+=(const Vector2D &v1)
{
	return this->Add(v1);
}

Vector2D& Vector2D::operator-=(const Vector2D &v1)
{
	return this->Substract(v1);
}

Vector2D& Vector2D::operator*=(const Vector2D &v1)
{
	return this->Multiply(v1);
}

Vector2D& Vector2D::operator/=(const Vector2D &v1)
{
	return this->Divide(v1);
}

Vector2D& Vector2D::operator*=(const int& i)
{
	this->x *= i;
	this->y *= i;
	return *this;
}


