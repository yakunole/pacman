//
//  Vector2D.hpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#ifndef Vector2D_hpp
#define Vector2D_hpp

/// Class which represents simple coordinates and allows to make arithmetical operations with them
class Vector2D
{
public:
	float x;
	float y;
	
	Vector2D();
	Vector2D( float x , float y );
	Vector2D& Add( const Vector2D& vec );
	Vector2D& Substract( const Vector2D& vec );
	Vector2D& Multiply( const Vector2D& vec );
	Vector2D& Divide ( const Vector2D& vec );
	
	Vector2D& operator+= ( const Vector2D& v1 );
	Vector2D& operator-= ( const Vector2D& v1 );
	Vector2D& operator*= ( const Vector2D& v1 );
	Vector2D& operator*= ( const int& i);
	Vector2D& operator/= ( const Vector2D& v1 );
	
	friend Vector2D& operator+ ( Vector2D& v1 , const Vector2D& v2 );
	friend Vector2D& operator- ( Vector2D& v1 , const Vector2D& v2 );
	friend Vector2D& operator* ( Vector2D& v1 , const Vector2D& v2 );
	friend Vector2D& operator/ ( Vector2D& v1 , const Vector2D& v2 );
};

#endif /* Vector2D_hpp */
