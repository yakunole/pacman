//
//  Transform.hpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#ifndef Transform_hpp
#define Transform_hpp

#include "Vector2D.hpp"

/// Class which contains both position and velocity vectors
class Transform
{
public:
	/// Intialize vectors with given speed and coordinates
	Transform( int x , int y, int speed );
	/// Default destructor
	~Transform();
	/// Initialization
	void Init();
	/// Update the position
	void Update();
	/// Update the position( if there was collison with wall )
	void Backup();
	
	/// return position vector
	Vector2D& GetPos(){ return position; }
	
	/// return velocity vector for x axis
	float& GetVelocityx (){ return velocity.x; }
	/// return velocity vector fot y axis
	float& GetVelocityy (){ return velocity.y; }
	
private:
	
	Vector2D position;
	Vector2D velocity;
	
	int height;
	int width;
	
	int speed;
};

#endif /* Transform_hpp */
