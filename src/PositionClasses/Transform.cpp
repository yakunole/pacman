//
//  Transform.cpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#include "Transform.hpp"

Transform::Transform( int x , int y, int speed )
{
	height =  width = 32;
	this->speed = speed;
	position.x = x;
	position.y = y;
	Init();
}

Transform::~Transform()
{
	
}

void Transform::Init()
{
	velocity.x = 0;
	velocity.y = 0;
}

void Transform::Update()
{
	position.x += velocity.x * speed;
	position.y += velocity.y * speed;
}

void Transform::Backup()
{
	position.x -= velocity.x * speed;
	position.y -= velocity.y * speed;
}

