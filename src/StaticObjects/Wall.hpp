//
//  Wall.hpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#ifndef Wall_hpp
#define Wall_hpp

#include "BaseClass.hpp"
#include "../DoubleDispatch.hpp"

/// Class which represents Wall
class Wall : public GameObject
{
public:
	/// Default constructor
	Wall(){ }
	/// Default destructor
	~Wall(){}
	/// Method to implement double dispatch
	virtual void Visit( struct Visitor *visitor ){ visitor->Visit(this); }
};

#endif /* Wall_hpp */
