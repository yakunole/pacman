//
//  Cherry.hpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#ifndef Cherry_hpp
#define Cherry_hpp

#include "BaseClass.hpp"
#include "../DoubleDispatch.hpp"

/// Class which represents cherry
class Cherry : public GameObject
{
public:
	/// Default constructor
	Cherry(){ }
	/// Default destructor
	~Cherry(){}
	/// Method to implement double dispatch
	virtual void Visit( struct Visitor *visitor ){ visitor->Visit(this); }
};

#endif /* Cherry_hpp */
