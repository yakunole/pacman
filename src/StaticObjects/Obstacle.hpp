//
//  Obstacle.hpp
//  yakunole
//
//  Created by Alex Yakunin on 14.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#ifndef Obstacle_hpp
#define Obstacle_hpp


#include "BaseClass.hpp"
#include "../DoubleDispatch.hpp"

/// Class which represents obstacle
class Obstacle : public GameObject
{
public:
	/// Default constructor
	Obstacle(){ }
	/// Default destructor
	~Obstacle(){}
	/// Method to implement double dispatch
	virtual void Visit( struct Visitor *visitor ){ visitor->Visit(this); }
};

#endif /* Obstacle_hpp */
