//
//  BaseClass.cpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#include "BaseClass.hpp"

GameObject::GameObject()
{
	
}

GameObject::~GameObject()
{
	
}

void GameObject::Init(int x, int y)
{
	m_destRect.w = m_destRect.h = m_srcRect.w = m_srcRect.h = 32;
	collisionRect.w = collisionRect.h = 30;
	m_srcRect.x = m_srcRect.y = 0;
	m_destRect.x = collisionRect.x = x;
	m_destRect.y = collisionRect.y =  y;
}

SDL_Rect GameObject::GetSrcRect()
{
	return m_srcRect;
}

SDL_Rect GameObject::GetDestRect()
{
	return  m_destRect;
}
