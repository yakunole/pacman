//
//  GhostHouse.hpp
//  yakunole
//
//  Created by Alex Yakunin on 14.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#ifndef GhostHouse_hpp
#define GhostHouse_hpp

#include <SDL2/SDL.h>

/// Class which represent ghost house
class GhostHouse
{
public:
	/// Default constructor
	GhostHouse(){}
	/// Default destructor
	~GhostHouse(){}
	/// Intialize an object
	void Init( int x, int y){ destRect.x = x; destRect.y = y; }
	/// Return destination rectangle
	SDL_Rect GetDestRect(){ return  destRect; }
private:
	SDL_Rect destRect;
};


#endif /* GhostHouse_hpp */
