//
//  Coin.hpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#ifndef Coin_hpp
#define Coin_hpp

#include "BaseClass.hpp"
#include "../DoubleDispatch.hpp"

/// Class which represents coin
class Coin : public GameObject
{
public:
	/// Default constructor
	Coin(){ }
	/// Default destructor
	~Coin(){}
	/// Method to implement double dispatch
	virtual void Visit( struct Visitor *visitor ){ visitor->Visit(this); }
};

#endif /* Coin_hpp */
