//
//  Gate.hpp
//  yakunole
//
//  Created by Alex Yakunin on 14.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#ifndef Gate_hpp
#define Gate_hpp

#include "BaseClass.hpp"
#include "../DoubleDispatch.hpp"

/// Class which represents Gate
class Gate : public GameObject
{
public:
	/// Default constructor
	Gate(){ }
	/// Default destructor
	~Gate(){}
	/// Method to implement double dispatch
	virtual void Visit( struct Visitor *visitor ){ visitor->Visit(this); }
};

#endif /* Gate_hpp */
