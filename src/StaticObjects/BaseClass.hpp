//
//  BaseClass.hpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#ifndef BaseClass_hpp
#define BaseClass_hpp

#include <SDL2/SDL.h>

/// BaseClass for static objects
///
/// Class is used to store game objects in same polymorphic container
class GameObject
{
public:
	/// Default constructor
	GameObject();
	/// Default destructor
	virtual ~GameObject();
	/// Initialize game object with given coordinates
	void Init(int x, int y);
	/// Method to implement double dispatch
	virtual void Visit( struct Visitor* visitor ) = 0;
	/// Method to return  source rectangle
	SDL_Rect GetSrcRect();
	/// Method to return destination rectangle
	SDL_Rect GetDestRect();
	/// Method to return collision rectangle
	SDL_Rect GetCollision(){ return collisionRect; }
protected:
	SDL_Rect m_srcRect, m_destRect, collisionRect;
};

#endif /* BaseClass_hpp */
