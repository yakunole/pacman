//
//  Map.cpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#include "Map.hpp"

Map::Map(const char* path) : mapPath(path)
{
	
}

Map::~Map()
{
	for( auto it = walls.begin() ; it != walls.end() ; ++it )
		delete  *it;
	for( auto j = colliders.begin() ; j != colliders.end() ; ++j )
		delete *j;
}

void Map::LoadMap(int sizeX, int sizeY, Pacman* player,  Purple* purple, Red* red, Yellow* yellow, Blue* blue, GhostHouse* gh)
{
	
	char tile;
	std::fstream mapFile;
	mapFile.open(mapPath);
	
	map.resize(sizeY);
	for( int i = 0 ; i < sizeY ; i++ )
		map[i].resize(sizeX);
	
	
	for( int y = 0 ; y < sizeY ; y++ )
	{
		for( int x = 0 ; x < sizeX ; x++ )
		{
			mapFile.get(tile);
			if( tile == 'x' )
			{
				map[y][x] = '0';
				gh->Init(x*32, y*32);
				mapFile.ignore();
				continue;
			}
			
			if( tile == 'g' )
			{
				GameObject* obj = new Gate();
				obj->Init(x*32, y*32);
				map[y][x] = '0';
				walls.push_back(obj);
				mapFile.ignore();
				continue;
			}
			AddTile(atoi(&tile), x, y, player, purple, red, yellow, blue);
			mapFile.ignore();
		}
	}
	mapFile.close();
}

void Map::AddTile(int id, int x, int y, Pacman* player,  Purple* purple, Red* red, Yellow* yellow, Blue* blue)
{
	GameObject* object;
	
	switch (id) {
		case 0:
			map[y][x] = '0';
			break;
		case 1:
			object = new Wall();
			object->Init(x*32, y*32);
			map[y][x] = '1';
			walls.push_back(object);
			break;
		case 2:
			object = new Coin();
			object->Init(x*32, y*32);
			map[y][x] = '2';
			colliders.push_back(object);
			break;
		case 3:
			map[y][x] = '0';
			player->Init(x*32, y*32);
			break;
		case 4:
			map[y][x] = '0';
			purple->Init(x*32, y*32);
			break;
		case 5:
			map[y][x] = '0';
			red->Init(x*32, y*32);
			break;
		case 6:
			map[y][x] = '0';
			yellow->Init(x*32, y*32);
			break;
		case 7:
			map[y][x] = '0';
			blue->Init(x*32, y*32);
			break;
		case 8:
			map[y][x] = '0';
			object = new Cherry();
			object->Init(x*32, y*32);
			colliders.push_back(object);
			break;
		case 9:
			map[y][x] = '0';
			object = new Obstacle();
			object->Init(x*32, y*32);
			colliders.push_back(object);
			break;
		default:
			break;
	}
}
