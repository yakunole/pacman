//
//  RedGhost.cpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#include "RedGhost.hpp"

Red::Red(int speed, int sDuration, int cDuration )
{
	homex = 34;
	homey = 3;
	this->speed = 64/speed;
	this->sDuration = sDuration;
	this->cDuration = cDuration;
}


void Red::Update(std::vector<std::vector<char> > map, int x, int y, int g, int j, std::string direction)
{
	if( scatter == 1 )
	{
		Scatter(map, g, j);
		return;
	}
	int resultx = x, resulty = y;
	m_direction = direction;
	if( direction == "right" )
	{
		if( y+4 < 35 && map[x][y+1] != '1' && map[x][y+2] != '1' &&  map[x][y+4] != '1' && map[x][y+4] != '1')
		{
			resulty += 4;
		}
	}
	
	else if( direction == "left" )
	{
		if( y-4 >= 0 && map[x][y-1] != '1' && map[x][y-2] != '1' && map[x][y-3] != '1' && map[x][y-4] != '1')
		{
			resulty -= 4;
		}
	}
	
	else if( direction == "up" )
	{
		if( x-4 >= 0 && map[x-1][y] != '1' && map[x-2][y] != '1' && map[x-3][y] != '1' && map[x-4][y] != '1')
		{
			resultx -= 4;
		}
	}
	
	else if( direction == "down" )
	{
		if( x+4 < 25  && map[x+1][y] != '1' && map[x+2][y] != '1' && map[x+3][y] != '1' && map[x+4][y] != '1')
		{
			resultx += 4;
		}
	}
	
	std::vector<QItem> a = BFS(map, resultx, resulty, g, j);
	flag = 1;
	previousx = destRect.x;
	previousy = destRect.y;
	if( a.size() > 0 )
	{
		auto j = a.end() - 1;
		if( j->col > destRect.x/32 )
			destRect.x += 32;
		else if(  j->col < destRect.x/32)
			destRect.x -= 32;
		
		
		if( j->row > destRect.y/32 )
			destRect.y += 32;
		else if( j->row < destRect.y/32 )
			destRect.y -= 32;
	}
}
