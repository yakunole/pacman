//
//  YellowGhost.hpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#ifndef YellowGhost_hpp
#define YellowGhost_hpp

#include "Enemy.hpp"

/// Class which represents an yellow ghost
class Yellow : public Enemy
{
public:
	/// Constructor which saves the needed speed
	Yellow(int speed);
	/// Update the position
	virtual void Update(std::vector<std::vector<char> > map, int x, int y, int g, int j, std::string direction);
private:
	int home;
};

#endif /* YellowGhost_hpp */
