//
//  BlueGhost.hpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#ifndef BlueGhost_hpp
#define BlueGhost_hpp

#include "Enemy.hpp"

/// Class which represents a blue ghost
class Blue : public Enemy
{
public:
	/// Constructor which saves needed speed and duration of both modes
	Blue(int speed, int sDuration, int cDuration );
	/// Update the position of a ghost
	virtual void Update(std::vector<std::vector<char> > map, int x, int y, int g, int j, std::string direction);
};

#endif /* BlueGhost_hpp */
