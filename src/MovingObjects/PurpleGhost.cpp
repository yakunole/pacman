//
//  PurpleGhost.cpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#include "PurpleGhost.hpp"

Purple::Purple(int speed, int sDuration, int cDuration)
{
	homex = 1;
	homey = 5;
	this->speed = 64/speed;
	this->sDuration = sDuration;
	this->cDuration = cDuration;
}

void Purple::Update(std::vector<std::vector<char> > map, int x, int y, int g, int j, std::string direction)
{
	if( scatter == 1 )
	{
		Scatter(map, g, j);
		return;
	}
	
	std::vector<QItem> a = BFS( map,  x,  y,  g, j);
	flag = 1;
	previousx = destRect.x;
	previousy = destRect.y;
	if( a.size() > 0 )
	{
		auto it = a.end() - 1;
		
		if( it->col > j )
			destRect.x += 32;
		else if(  it->col < j)
			destRect.x -= 32;
		
		
		if( it->row > g )
			destRect.y += 32;
		else if( it->row < g)
			destRect.y -= 32;
		
	}
}

