//
//  Enemy.hpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#ifndef Enemy_hpp
#define Enemy_hpp

#include <queue>
#include <vector>
#include <set>
#include <memory>
#include <string>
#include <SDL2/SDL.h>
#include <iostream>

/// Class which represents an item in a 2d array
class QItem
{
public:
	int row;
	int col;
	int dist;
	QItem(int x, int y, int w)
	: row(x), col(y), dist(w)
	{
	}
	QItem(){}
};

/// Class which represents an enemy
class Enemy
{
public:
	/// Default constructor
	Enemy(){}
	Enemy( int x, int y){}
	/// Default destructor
	virtual ~Enemy(){}
	/// Initalize an enemy
	void Init(int x, int y);
	/// Update the position of an enemy
	virtual void Update(std::vector<std::vector<char> > map, int x, int y, int g, int j, std::string direction) = 0;
	/// find if there exists path to a target and save the path
	std::vector<std::vector<std::shared_ptr<QItem> > > findPath(std::vector<std::vector<char> > map, int g, int h, int x, int y);
	/// Combine findPath and printPath in order to give a result
	std::vector<QItem> BFS(std::vector<std::vector<char> > map, int x, int y, int g, int j);
	/// make a path from the output of fidPath method
	std::vector<QItem> printPath(int x, int y, int g, int j, std::vector<std::vector<std::shared_ptr<QItem> > >);
	/// Behaviour in a Scatter mode
	void Scatter(std::vector<std::vector<char> > map, int g, int j);
	/// return counter
	int GetCounter(){ return counter; }
	/// Increment counter
	void IncCounter(){ counter++; }
	/// Make counter equal to zero and check if it is a time to change a mode
	void AnnulCounter();
	/// Make counter equal to zero when frightened
	void ftightenCounter(){ counter = 0; mode = 0; }
	/// return destination rectangle
	SDL_Rect GetDestRect(){ return destRect; }
	/// return source rectangle
	SDL_Rect GetSrcRect(){ return srcRect; }
	/// return frightened flag
	int& isFrightened(){ return frightened; }
	/// return scatter flag
	int& isScattered(){ return scatter; }
	/// check if it is time to make a move
	bool Time();
protected:
	SDL_Rect srcRect, destRect, chaseRect;
	int counter;
	int flag;
	int previousx;
	int previousy;
	int scatter;
	int homex;
	int homey;
	int speed;
	int sDuration;
	int cDuration;
	int time;
	int mode;
	int frightened;
};

#endif /* Enemy_hpp */
