//
//  KeyboardController.cpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#include "KeyboardController.hpp"

KeyboardController::KeyboardController(Pacman* player)
{
	pacman = player;
}

KeyboardController::~KeyboardController()
{
	delete pacman;
}

void KeyboardController::Update(SDL_Event* event)
{
	if ( event->type == SDL_KEYDOWN )
	{
		switch(event->key.keysym.sym)
		{
			case SDLK_w:
				pacman->GetCoordinates()->GetVelocityy() = -1;
				pacman->GetDirection() = "up";
				break;
			case SDLK_s:
				pacman->GetCoordinates()->GetVelocityy() = 1;
				pacman->GetDirection() = "down";
				break;
			case SDLK_a:
				pacman->GetCoordinates()->GetVelocityx() = -1;
				pacman->GetDirection() = "left";
				break;
			case SDLK_d:
				pacman->GetCoordinates()->GetVelocityx() = 1;
				pacman->GetDirection() = "rigth";
				break;
			default:
				break;
		}
	}
	if ( event->type == SDL_KEYUP ){
		switch(event->key.keysym.sym)
		{
			case SDLK_w:
				pacman->GetCoordinates()->GetVelocityy() = 0;
				break;
			case SDLK_s:
				pacman->GetCoordinates()->GetVelocityy() = 0;
				break;
			case SDLK_a:
				pacman->GetCoordinates()->GetVelocityx() = 0;
				break;
			case SDLK_d:
				pacman->GetCoordinates()->GetVelocityx() = 0;
				break;
			default:
				break;
		}
	}
	pacman->GetCoordinates()->Update();
}

