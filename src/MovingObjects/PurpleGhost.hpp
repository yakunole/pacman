//
//  PurpleGhost.hpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#ifndef PurpleGhost_hpp
#define PurpleGhost_hpp

#include "Enemy.hpp"

/// Class which represent a purple ghost
class Purple : public Enemy
{
public:
	/// Constructor which saves needed speed and duration of both modes
	Purple(int speed, int sDuration, int cDuration);
	/// Update the position of a ghost
	virtual void Update(std::vector<std::vector<char> > map, int x, int y, int g, int j, std::string direction);
};

#endif /* PurpleGhost_hpp */
