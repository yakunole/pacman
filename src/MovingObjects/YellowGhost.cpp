//
//  YellowGhost.cpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#include "YellowGhost.hpp"

Yellow::Yellow(int speed )
{
	homex = 1;
	homey = 21;
	this->speed = 64/speed;
	sDuration = 0;
	cDuration = 15;
}

void Yellow::Update(std::vector<std::vector<char> > map, int x, int y, int g, int j, std::string direction)
{
	if( scatter == 1 )
	{
		Scatter(map, g, j);
		return;
	}
	std::vector<QItem> a = BFS( map,  x,  y,  g, j);
	previousx = destRect.x;
	previousy = destRect.y;
	flag = 1;
	if( g == homey && j == homex )
		home = 0;
	if( a.size() > 12 )
		home = 0;
	if( a.size() < 6 || home == 1 )
	{
		std::vector<QItem> a = BFS( map, homey,  homex,  g, j);
		if( a.size() > 0)
		{
			auto j = a.end() - 1;
			
			if( j->col > destRect.x/32 )
				destRect.x += 32;
			else if(  j->col < destRect.x/32)
				destRect.x -= 32;
			
			
			if( j->row > destRect.y/32 )
				destRect.y += 32;
			else if( j->row < destRect.y/32 )
				destRect.y -= 32;
			home = 1;
		}
	}
	if( a.size() > 0 && home == 0 )
	{
		auto j = a.end() - 1;
		if( j->col > destRect.x/32 )
			destRect.x += 32;
		else if(  j->col < destRect.x/32)
			destRect.x -= 32;
		
		
		if( j->row > destRect.y/32 )
			destRect.y += 32;
		else if( j->row < destRect.y/32 )
			destRect.y -= 32;
		
	}
}


