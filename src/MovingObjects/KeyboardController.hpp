//
//  KeyboardController.hpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#ifndef KeyboardController_hpp
#define KeyboardController_hpp

#include <SDL2/SDL.h>
#include "Pacman.hpp"
#include <string>

/// Class which handles user input
class KeyboardController
{
public:
	/// Constructor
	KeyboardController(Pacman* player);
	/// Destructor
	~KeyboardController();
	/// Check for new input
	void Update(SDL_Event* event);
private:
	Pacman* pacman;
};

#endif /* KeyboardController_hpp */
