//
//  RedGhost.hpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#ifndef RedGhost_hpp
#define RedGhost_hpp

#include "Enemy.hpp"

/// Class which represents a red ghost
class Red : public Enemy
{
public:
	/// Constructor which saves needed speed and duration of both modes
	Red(int speed, int sDuration, int cDuration );
	/// Update the position of a ghost
	virtual void Update(std::vector<std::vector<char> > map, int x, int y, int g, int j, std::string direction);
private:
	std::string m_direction;
};

#endif /* RedGhost_hpp */
