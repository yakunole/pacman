//
//  Enemy.cpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#include "Enemy.hpp"

void Enemy::Init(int x, int y)
{
	srcRect.w = srcRect.h = destRect.w = destRect.h = chaseRect.w = chaseRect.h =  32;
	srcRect.x = srcRect.y = counter = previousx = previousy = flag = time = mode = scatter = frightened = 0;
	destRect.x = chaseRect.x = x ;
	destRect.y = chaseRect.y =  y;
}

std::vector<std::vector<std::shared_ptr<QItem> > > Enemy::findPath(std::vector<std::vector<char> > map, int g, int h, int x, int y)
{
	std::vector<std::vector<std::shared_ptr<QItem> > > pred;
	std::queue<std::shared_ptr<QItem> > q;
	bool visited[25][36];
	
	QItem source(0, 0, 0);
	pred.resize(25);
	for (int i = 0; i < 25; i++) {
		pred[i].resize(36);
		for (int j = 0; j < 36; j++)
		{
			if ( map[i][j] == '1')
				visited[i][j] = true;
			else
				visited[i][j] = false;
			
			if ( i == g && j == h )
			{
				source.row = i;
				source.col = j;
			}
		}
	}
	
	
	
	std::shared_ptr<QItem> m_ptr = std::make_shared<QItem>(source);
	q.push(m_ptr);
	visited[m_ptr->row][m_ptr->col] = true;
	if( flag == 1 )
		visited[previousy/32][previousx/32] = true;
	
	while(!q.empty())
	{
		std::shared_ptr<QItem> current = q.front();
		q.pop();
		
		if( current->row == x && current->col == y )
		{
			return pred;
		}
		
		if(current->row-1 >= 0 && visited[current->row-1][current->col] == false  )
		{
			QItem a(current->row-1, current->col, current->dist+1);
			q.push(std::make_shared<QItem>(a));
			visited[current->row-1][current->col] = true;
			pred[current->row-1][current->col] = current;
		}
		
		if(current->row + 1 < 25 && visited[current->row+1][current->col] == false  )
		{
			QItem a(current->row+1, current->col, current->dist+1);
			q.push(std::make_shared<QItem>(a));
			visited[current->row+1][current->col] = true;
			pred[current->row+1][current->col] = current;
		}
		
		if(current->col-1 >= 0 && visited[current->row][current->col-1] == false )
		{
			QItem a(current->row, current->col-1, current->dist+1);
			q.push(std::make_shared<QItem>(a));
			visited[current->row][current->col-1] = true;
			pred[current->row][current->col-1] = current;
		}
		
		if(current->col + 1 < 36 && visited[current->row][current->col+1] == false  )
		{
			QItem a(current->row, current->col+1, current->dist+1);
			q.push(std::make_shared<QItem>(a));
			visited[current->row][current->col+1] = true;
			pred[current->row][current->col+1] = current;
		}
	}
	
	return pred;
}



std::vector<QItem> Enemy::BFS(std::vector<std::vector<char> > map, int x, int y, int g, int j)
{
	std::vector<std::vector<std::shared_ptr<QItem> > > vec;
	vec = findPath(map, g, j, x, y);
	
	return printPath(x, y, g, j, vec);
}


std::vector<QItem> Enemy::printPath(int x, int y, int g, int j, std::vector<std::vector<std::shared_ptr<QItem> > > a)
{
	std::vector<QItem> path;
	int xcor, ycor;
	int temp = x;
	xcor = x;
	ycor =y;
	if( xcor < 0 || xcor > 24 || ycor < 0 || ycor > 35)
		return path;
	
	while(a[xcor][ycor] != nullptr)
	{
		
		
		path.push_back(QItem(xcor,ycor,0));
		
		
		
		if(a[xcor][ycor]->row == g && a[xcor][ycor]->col == j )
		{
			break;
		}
		
		
		temp = a[xcor][ycor]->row;
		ycor = a[xcor][ycor]->col;
		xcor = temp;
		
	}
	
	return path;
}


void Enemy::Scatter(std::vector<std::vector<char> > map, int g, int j)
{
	std::vector<QItem> a = BFS( map,  homey,  homex,  g, j);
	if( frightened == 1 && a.size() == 0 )
	{
		scatter = 0;
		frightened = 0;
	}
	flag = 1;
	previousx = destRect.x;
	previousy = destRect.y;
	if( a.size() > 0 )
	{
		auto j = a.end() - 1;
		if( j->col > destRect.x/32 )
			destRect.x += 32;
		else if(  j->col < destRect.x/32)
			destRect.x -= 32;
		
		
		if( j->row > destRect.y/32 )
			destRect.y += 32;
		else if( j->row < destRect.y/32 )
			destRect.y -= 32;
		
	}
}

bool Enemy::Time()
{
	if( counter % speed == 0 )
		return true;
	return false;
}


void Enemy::AnnulCounter()
{
	counter = 0;
	if( mode == 0 )
	{
		time++;
		if( time >= cDuration )
		{
			
			time = 0;
			mode = 1;
			scatter = 1;
		}
	}
	
	else if( mode == 1 )
	{
		time++;
		if(time >= sDuration )
		{
			time = 0;
			mode = 0;
			scatter = 0;
		}
	}
}
