//
//  Pacman.cpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#include "Pacman.hpp"

Pacman::Pacman(int speed)
{
	this->speed = speed;
	srcRect.x = srcRect.y = 0;
	srcRect.h = srcRect.w = destRect.w = destRect.h = 32;
	collisionRect.h = collisionRect.w = 30;
	counter = 0;
}

void Pacman::Init(int x, int y)
{
	destRect.x =  x;
	destRect.y = y;
	transform = new Transform(x ,y, speed);
}


Pacman::~Pacman()
{
	delete transform;
}


void Pacman::Update()
{
	destRect.x = collisionRect.x = transform->GetPos().x;
	destRect.y = collisionRect.y = transform->GetPos().y;
}

void Pacman::Hit()
{
	transform->Backup();
	destRect.x = transform->GetPos().x;
	destRect.y = transform->GetPos().y;
	collisionRect.x = destRect.x;
	collisionRect.y = destRect.y;
}

Transform* Pacman::GetCoordinates()
{
	return transform;
}

