//
//  Pacman.hpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#ifndef Pacman_hpp
#define Pacman_hpp

#include <iostream>
#include "../PositionClasses/Transform.hpp"
#include <SDL2/SDL.h>

/// Class which represents pacman
class Pacman
{
public:
	/// Constructor which takes speed as a parameter
	Pacman(int speed);
	/// Default desturtor
	~Pacman();
	/// Update the position of pacman
	void Update();
	/// Initalize Pacman
	void Init(int x, int y);
	/// If there was a hit with the wall
	void Hit();
	
	/// return source rectangle
	SDL_Rect GetSource(){ return srcRect; }
	/// return destination rectangle
	SDL_Rect& GetDest(){ return destRect; }
	/// return collision rectangle
	SDL_Rect GetCollision(){ return collisionRect; }
	/// return direction of a pacman
	std::string& GetDirection(){ return direction; }
	/// return vector with velocity and position
	Transform* GetCoordinates();
	friend class Map;
private:
	SDL_Rect srcRect, destRect, collisionRect;
	int counter;
	Transform *transform;
	std::string direction;
	int speed;
};

#endif /* Pacman_hpp */
