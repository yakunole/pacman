//
//  AssetManager.hpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#ifndef AssetManager_hpp
#define AssetManager_hpp

#include "TextureManager.hpp"
#include <SDL2/SDL_ttf.h>
#include <string>
#include <map>

/// Class which holds all textures and fonts
class AssetManager
{
public:
	/// Default constructor
	AssetManager();
	/// Destructor
	~AssetManager();
	/// Add texture to a map
	void AddTexture(std::string id, const char* path, SDL_Renderer* renderer);
	/// Get texture from a map
	SDL_Texture* GetTexture(std::string id);
	/// Add font to a map
	void AddFont(std::string id, std::string path, int fontSize);
	/// Get font form a map
	TTF_Font* GetFont(std::string id);
private:
	std::map<std::string, SDL_Texture*> textures;
	std::map<std::string, TTF_Font*> fonts;
};


#endif /* AssetManager_hpp */
