//
//  Collision.cpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#include "Collision.hpp"

bool Collision::AABB(const SDL_Rect &recA, const SDL_Rect &recB)
{
	if (
		recA.x + recA.w - 2 >= recB.x &&
		recB.x + recB.w - 2 >= recA.x &&
		recA.y + recA.h - 2 >= recB.y &&
		recB.y + recB.h - 2 >= recA.y
		)
	{
		return true;
	}
	return false;
}

