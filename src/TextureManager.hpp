//
//  TextureManager.hpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#ifndef TextureManager_hpp
#define TextureManager_hpp

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

/// Class is made for loading imaages from files and making sdl textures out of them. It is also used for displaying images on screen
class TextureManager
{
public:
	/// Load image from file and make texture out of an image
	static SDL_Texture* LoadTexture( const char* filename, SDL_Renderer* renderer);
	/// Draw texture on the screen
	static void Draw(SDL_Texture* tex, SDL_Renderer* render, SDL_Rect src, SDL_Rect dest);
};

#endif /* TextureManager_hpp */
