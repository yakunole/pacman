//
//  UILabel.cpp
//  yakunole
//
//  Created by Alex Yakunin on 14.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#include "UILabel.hpp"

UILabel::UILabel( int xpos, int ypos, std::string text, std::string font, SDL_Color& colour, AssetManager* asset, SDL_Renderer* rend) : labelText(text), labelFont(font), textColour(colour), render(rend), assets(asset)
{
	position.x = xpos;
	position.y = ypos;
	
	SetLabelText(labelText, labelFont);
}



UILabel::~UILabel()
{
	SDL_DestroyTexture( labelTexture );
}

void UILabel::SetLabelText(std::string text, std::string font)
{
	SDL_Surface* surf = TTF_RenderText_Blended(assets->GetFont(font), text.c_str(), textColour);
	labelTexture = SDL_CreateTextureFromSurface(render, surf);
	SDL_FreeSurface(surf);
	SDL_QueryTexture(labelTexture, nullptr, nullptr, &position.w, &position.h);
}


void UILabel::Draw()
{
	SDL_RenderCopy(render, labelTexture, nullptr, &position);
}


