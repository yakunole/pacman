//
//  Game.hpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#ifndef Game_hpp
#define Game_hpp

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>
#include "Map.hpp"
#include "DoubleDispatch.hpp"
#include "TextureManager.hpp"
#include "AssetManager.hpp"
#include "MovingObjects/Pacman.hpp"
#include "MovingObjects/KeyboardController.hpp"
#include "Collision.hpp"
#include "MovingObjects/PurpleGhost.hpp"
#include "MovingObjects/RedGhost.hpp"
#include "MovingObjects/YellowGhost.hpp"
#include "MovingObjects/BlueGhost.hpp"
#include "UILabel.hpp"
#include "StaticObjects/GhostHouse.hpp"
#include <sstream>


/// Main class which is responsible for game screen
class Game
{
public:
	
	/// Constructor
	Game();
	
	/// Destructor
	~Game();
	
	/// Method to intialize game window with given name, width and height
	void Init( const char *title, int x, int y, int width, int height );
	
	/// Method to hadle user input
	void HandleEvents();
	
	/// Method to update positions of game objects
	void Update();
	
	/// Method to display textures on the screen
	void Render();
	
	/// Method to clean resources allocated by SDL
	void Clean();
	
	/// Load characteristics of objects from file
	void ConfigurGame( const char* path );

	/// Display main menu
	int mainMenu();
	
	/// Method which returns true if game is still running or false if not
	bool Running();
	
private:
	SDL_Window* window;
	SDL_Renderer* renderer;
	SDL_Event event;
	bool isRunning;
};


#endif /* Game_hpp */
