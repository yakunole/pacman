//
//  UILabel.hpp
//  yakunole
//
//  Created by Alex Yakunin on 14.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#ifndef UILabel_hpp
#define UILabel_hpp

#include <string>
#include "Game.hpp"

/// class which displays TTF text on the screen
class UILabel
{
public:
	/// Constructor which takes coordinates, text, font, color and renderer
	UILabel( int xpos, int ypos, std::string text, std::string font, SDL_Color& colour, AssetManager* asset, SDL_Renderer* rend);
	/// Default destructor
	~UILabel();
	/// Set text to a given one
	void SetLabelText(std::string text, std::string font);
	/// Draw text on a Screen
	void Draw();
	friend class Game;
private:
	SDL_Rect position;
	std::string labelText;
	std::string labelFont;
	SDL_Color textColour;
	SDL_Texture* labelTexture;
	SDL_Renderer* render;
	AssetManager* assets;
};

#endif /* UILabel_hpp */
