//
//  AssetManager.cpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#include "AssetManager.hpp"

AssetManager::AssetManager()
{
	
}

AssetManager::~AssetManager()
{
	for( auto it = textures.begin() ; it != textures.end() ; ++it )
	{
		SDL_DestroyTexture(it->second);
	}
	
	for( auto j = fonts.begin() ; j != fonts.end() ; ++j )
	{
		TTF_CloseFont( j->second );
	}
}

void AssetManager::AddTexture(std::string id, const char* path, SDL_Renderer* renderer)
{
	textures[id] = TextureManager::LoadTexture(path, renderer);
}

SDL_Texture* AssetManager::GetTexture(std::string id)
{
	return textures[id];
}

void AssetManager::AddFont(std::string id, std::string path, int fontSize)
{
	fonts[id] = TTF_OpenFont(path.c_str(), fontSize);
}


TTF_Font* AssetManager::GetFont(std::string id)
{
	return fonts[id];
}

