//
//  main.cpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#include "Game.hpp"

int main()
{
	Game* game = nullptr;
	
	const int FPS = 60;
	const int frameDelay = 1000/FPS;
	
	Uint32 frameStart;
	int frameTime;
	
	game = new Game();
	game->Init( "Pacman" , SDL_WINDOWPOS_CENTERED , SDL_WINDOWPOS_CENTERED , 1152 , 800 );
	game->mainMenu();
	while ( game->Running() )
	{
		frameStart = SDL_GetTicks();
		
		game->HandleEvents();
		game->Update();
		game->Render();
		
		frameTime = SDL_GetTicks() - frameStart;
		if ( frameDelay > frameTime )
		{
			SDL_Delay( frameDelay - frameTime );
		}
	}
	game->Clean();
	delete  game;
	return 0;
}

