//
//  TextureManager.cpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#include "TextureManager.hpp"

SDL_Texture* TextureManager::LoadTexture(const char *fileName, SDL_Renderer* render)
{
	SDL_Surface *tempSurface = IMG_Load( fileName );
	SDL_Texture * tex = SDL_CreateTextureFromSurface( render , tempSurface );
	SDL_FreeSurface( tempSurface );
	return tex;
}

void TextureManager::Draw(SDL_Texture *tex, SDL_Renderer* render, SDL_Rect src, SDL_Rect dest)
{
	SDL_RenderCopy( render , tex, &src, &dest);
}



