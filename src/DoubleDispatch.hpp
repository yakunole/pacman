//
//  DoubleDispatch.hpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#ifndef DoubleDispatch_hpp
#define DoubleDispatch_hpp

#include "StaticObjects/BaseClass.hpp"
#include <string>

class GameObject;


struct Visitor
{
	virtual void Visit( class Wall* ) = 0;
	virtual void Visit( class Coin* ) = 0;
	virtual void Visit( class Cherry* ) = 0;
	virtual void Visit( class Obstacle* ) = 0;
	virtual void Visit( class Gate* ) = 0;
};

/// Structure to understand with which children class we are  dealing
struct ReactVisitor : Visitor
{
	ReactVisitor( class Person *p ) : person(p) { }
	virtual void Visit( class Wall *w );
	virtual void Visit( class Coin *c );
	virtual void Visit( class Cherry *ch );
	virtual void Visit( class Obstacle *os );
	virtual void Visit( class Gate *g );
	class Person *person;
};

/// Class which reacts to a children class in order to its type
class Person
{
public:
	
	/// Method which send class to structure ReactVisitor
	void ReactTo( GameObject* object )
	{
		ReactVisitor visitor(this);
		object->Visit(&visitor);
	}
	/// Method to react if it is wall
	void Wall(){ result = "wall"; }
	/// Method to react if it is coin
	void Coin(){ result = "coin"; }
	/// Method to react if it is cherry
	void Cherry(){ result = "cherry"; }
	/// Method to react if it is obtacle
	void Obstacle(){ result = "obstacle"; }
	/// Method to react if it is gate
	void Gate(){ result = "gate"; }
	/// Method to return result string
	std::string Result(){ return result; }
private:
	std::string result;
};

#endif /* DoubleDispatch_hpp */
