//
//  Map.hpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#ifndef Map_hpp
#define Map_hpp

#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include "StaticObjects/BaseClass.hpp"
#include "StaticObjects/Wall.hpp"
#include "StaticObjects/Coin.hpp"
#include "MovingObjects/Pacman.hpp"
#include "MovingObjects/PurpleGhost.hpp"
#include "MovingObjects/RedGhost.hpp"
#include "MovingObjects/YellowGhost.hpp"
#include "MovingObjects/BlueGhost.hpp"
#include "StaticObjects/Cherry.hpp"
#include "StaticObjects/Obstacle.hpp"
#include "StaticObjects/GhostHouse.hpp"
#include "StaticObjects/Gate.hpp"

///  Class which reads map from file and stores objects to arrays
class Map
{
public:
	/// Constructor to save path to file
	Map(const char* path);
	/// Destructor
	~Map();
	/// Read map from file
	void LoadMap(int sizeX, int sizeY, Pacman* player,  Purple* purple, Red* red, Yellow* yellow, Blue* blue, GhostHouse* gh);
	/// Add tile to arrays
	void AddTile(int id, int x, int y, Pacman* player,  Purple* purple, Red* red, Yellow* yellow, Blue* blue);
	/// Return main map of the game
	std::vector<std::vector<char> > GetMap(){ return map; }
	/// Return all walls
	std::vector<GameObject*> GetWalls(){ return walls; }
	/// Return all colliders
	std::vector<GameObject*>& GetColliders(){ return colliders; }
private:
	const char* mapPath;
	std::vector<std::vector<char> > map;
	std::vector<GameObject*> walls;
	std::vector<GameObject*> colliders;
};


#endif /* Map_hpp */
