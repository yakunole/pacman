//
//  Collision.hpp
//  yakunole
//
//  Created by Alex Yakunin on 13.06.2021.
//  Copyright © 2021 Alex Yakunin. All rights reserved.
//

#ifndef Collision_hpp
#define Collision_hpp

#include <SDL2/SDL.h>

/// Class which checks for collison
class Collision
{
public:
	/// Method to check for simple rectangle collison
	static bool AABB( const SDL_Rect &recA , const SDL_Rect &recB );
};

#endif /* Collision_hpp */
