GXX=g++
FLAGS= -Wall -pedantic
OBJ=$(SRC)/Game.o $(SRC)/main.o $(SRC)/TextureManager.o $(SRC)/AssetManager.o $(SRC)/DoubleDispatch.o $(SRC)/$(ST)/BaseClass.o $(SRC)/Map.o $(SRC)/$(POS)/Vector2D.o $(SRC)/$(POS)/Transform.o $(SRC)/$(MOV)/Pacman.o $(SRC)/$(MOV)/KeyboardController.o $(SRC)/Collision.o $(SRC)/$(MOV)/Enemy.o $(SRC)/$(MOV)/PurpleGhost.o $(SRC)/$(MOV)/YellowGhost.o $(SRC)/$(MOV)/BlueGhost.o $(SRC)/$(MOV)/RedGhost.o $(SRC)/$(ST)/BaseClass.o $(SRC)/UILabel.o
SRC=src
ST=StaticObjects
POS=PositionClasses
MOV=MovingObjects

dep:
	g++ -MM  $(SRC)/*.cpp $(SRC)/$(POS)/*.cpp $(SRC)/$(ST)/*.cpp $(SRC)/$(MOV)/*.cpp  >> $(SRC)/dependencies.mk

include $(SRC)/dependencies.mk

$(SRC)/%.o: $(SRC)/%.cpp
	$(GXX) $< $(FLAGS) -c  -o $@

all: $(OBJ)
	make compile run clean

compile: $(OBJ)
	$(GXX)  $^ -o yakunole  -lSDL2 -lSDL2_image -lSDL2_mixer -lSDL2_ttf

run:
	./yakunole

doc:

index:
	touch doc/index.html
	echo '<meta http-equiv="REFRESH" content="0;URL=html/index.html">' > doc/index.html


clean:
	rm -f yakunole  $(OBJ)
